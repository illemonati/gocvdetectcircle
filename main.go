package main

import (
	"github.com/kbinani/screenshot"
	"gocv.io/x/gocv"
	"image"
	"image/color"
)

func getScreenShot() *image.RGBA {
	bounds := screenshot.GetDisplayBounds(0)
	img, _ := screenshot.CaptureRect(bounds)
	return img
}

func processImage(img *image.RGBA) {
	mat, _ := gocv.ImageToMatRGBA(img)
	window := gocv.NewWindow("Result")
	grey := gocv.NewMat()

	gocv.CvtColor(mat, &grey, gocv.ColorBGRAToGray)

	circles := gocv.NewMat()
	gocv.HoughCircles(grey, &circles, gocv.HoughGradient, 1.7, 10)

	for i := 0; i < circles.Cols(); i++ {
		v := circles.GetVecfAt(0, i)
		if len(v) > 2 {
			x := int(v[0])
			y := int(v[1])
			r := int(v[2])

			gocv.Circle(&mat, image.Pt(x, y), r, color.RGBA{0, 0, 255, 0}, 2)
			gocv.Circle(&mat, image.Pt(x, y), 2, color.RGBA{255, 0, 0, 0}, 3)
		}
	}

	for {
		window.IMShow(mat)
		if window.WaitKey(2) >= 0 {
			break
		}
	}
	// window.Close()
}

func main() {
	img := getScreenShot()
	processImage(img)
}
