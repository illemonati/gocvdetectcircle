module gitlab.com/illemonati/gocvDetectCircle

go 1.13

require (
	github.com/BurntSushi/xgb v0.0.0-20160522181843-27f122750802 // indirect
	github.com/gen2brain/shm v0.0.0-20191025110947-b09d223a76f1 // indirect
	github.com/kbinani/screenshot v0.0.0-20190719135742-f06580e30cdc
	github.com/lxn/win v0.0.0-20191128105842-2da648fda5b4 // indirect
	gocv.io/x/gocv v0.21.0
)
